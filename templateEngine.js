const fs = require('fs');
const os = require('os');


fs.readFile("./template.tmpl",'utf8' , function (err, data) {
	
	processTemplate(data);
});


function processTemplate(templateInput) {

	const data = {
		"Name": 'Yevgen',
		"Stuff" : [
		  {
		    "Thing" : "roses",
		    "Desc"  : "red"
		  },
		  {
		    "Thing" : "violets",
		    "Desc"  : "blue"
		  },
		  {
		    "Thing" : "you",
		    "Desc"  : "able to solve this"
		  },
		  {
		    "Thing" : "we",
		    "Desc"  : "interested in you"
		  }
		]
	};
	let naked = "";

	// #each operator should generate a foreach funciton that returns <p> </p> elements
	// if the operator is {{ then i should read its value and find it in json object
	// #unless should return if(!variable){} else{}  
	//if after {{ a-z, then is a value so we take it from our data set
	//else we .replace the #each with name of dataset + '.' + forEach(item => item. na

	const nakedEx = /{{([^}}]+)?}}/g;
	let templateOutput ="";
	let lines = templateInput.split(/[\r\n]+/g);
	let mappedThings = "";
	let inLoopTrigger = false;
	allHTML = "<html><body>";
	iterationHTML = "";
	
	lines.map(line => {

		//while loop to eliminate every word that is represented as js code
	
		let lineHTML = "<p>";

		while(naked = nakedEx.exec(line)){

			console.log(naked[1].substr(0,1))

			let spl = naked[1].split(" ");
			let key = data[spl[1]];
			
			if(naked[1].substr(0,1)=="#"){				
				console.log("loop starts");
				mappedThings = key.map(thing => thing);
				inLoopTrigger = true;
			}
			else if(inLoopTrigger){		
				lineHTML += "<ul>";	
				mappedThings.map( thing => { lineHTML="<li>"+lineHTML+(line.replace(naked[0],thing[naked[1]]))+"</li>" } );
				lineHTML += "</ul>";
				if(naked[1].substr(0,1)=="/"){
					console.log("loop ends");
					inLoopTrigger = false;
				}
			}
			else{
				console.log(line.replace(naked[0],data[naked[1]]));
			}	
		}
		iterationHTML = iterationHTML + lineHTML + "</p>"
	});	
    allHTML = allHTML + iterationHTML + "</body></html>"
    //console.log(allHTML);
}